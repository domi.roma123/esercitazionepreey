
using app.libreria as app from  '../db/cap-valid-data-model';

service CatalogService  {

 entity Books
    as projection on app.Books;

 entity Authors 
    as projection on  app.Authors;

  entity Publisher
    as projection on  app.Publisher;

   entity AuthorsBooks
    as projection on  app.AuthorsBooks;

  entity PublisherBooks
    as projection on  app.PublisherBooks;

}