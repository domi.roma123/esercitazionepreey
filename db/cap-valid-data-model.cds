namespace app.libreria;

using
{ cuid }
from '@sap/cds/common';


entity Books: cuid {
    Title: String;
    ISBN: String;
    Publication_Date: Date;
    Genre: String;
    Description: String;
    Number_of_Pages: Integer;
    Language: String;
    Average_Rating: Double;
    Cover_Image_URL:  String;
    Authors: Association to many AuthorsBooks on Authors.book = $self;
    Publisher: Association to one PublisherBooks on Publisher.book = $self;
    stock: Integer;
}

entity Authors : cuid {
    First_Name: String;
    Last_Name: String;
    Date_of_Birth: Date;
    Nationality: String;
    Biography: String;
    Major_Works: String;
    Image_URL: String;
    Books: Association to many AuthorsBooks on Books.author = $self; 
}

entity AuthorsBooks : cuid {
    key book: Association to Books;
    key author: Association to Authors;
}

entity Publisher : cuid {
    Publisher_Name:String;
    Location: String;
    Description: String;
    Books: Association to many PublisherBooks on Books.publisher = $self; 
}

entity PublisherBooks : cuid {
    key book: Association to Books;
    key publisher: Association to Publisher;
}

