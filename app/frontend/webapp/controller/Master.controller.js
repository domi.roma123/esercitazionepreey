sap.ui.define([
    "./BaseController",
    "sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
    './utils/formatter',
    "sap/ui/model/json/JSONModel"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (BaseController, Filter, FilterOperator, formatter, JSONModel) {
        "use strict";

        return BaseController.extend("frontend.controller.Master", {
            formatter: formatter,
            onInit: function () {
                // this.getView().byId("gridList").addStyleClass("disabledItem")
            }, 
            onAfterRendering: function() {
                // Accedi ai dati dopo che la vista è stata completamente renderizzata
                var oModel = new sap.ui.model.odata.v4.ODataModel({
                    serviceUrl: "/odata/v4/catalog/",
                    synchronizationMode: "None"
                });
                
                // Effettua una richiesta per leggere tutti i record dell'entità "Books"
                oModel.bindList("/Books").requestContexts().then(function(aContexts) {
                    aContexts.forEach(function(oContext) {
                        var oBook = oContext.getObject();
                        // Gestisci i dati del libro
                        console.log("Dati del libro:", oBook);
                    });
                }).catch(function(oError) {
                    console.error("Errore durante la lettura dei dati:", oError);
                });
            },
                    
   
            onNavToBooks: function(){
                this.getRouter().navTo("books");
            },
            onPress : function(oEvent){
                var oItem, oCtx;
                oItem = oEvent.getSource();
                oCtx = oItem.getBindingContext();
                this.getRouter().navTo("author",{
                    authorId : oCtx.getProperty("ID")
                });
            },
            onSearch: function (oEvent) {
                // add filter for search
                var aFilters = [];
                var sQuery = oEvent.getSource().getValue();
                if (sQuery && sQuery.length > 0) {
                    var filter = new Filter("Title", FilterOperator.Contains, sQuery);
                    aFilters.push(filter);
                }
    
                // update list binding
                var oList = this.byId("gridList");
                var oBinding = oList.getBinding("items");
                oBinding.filter(aFilters, "Application");
            }
        });
    });
