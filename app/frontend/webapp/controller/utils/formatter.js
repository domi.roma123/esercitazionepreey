sap.ui.define(function() {
	"use strict";

	var formatter = {

		HighlightState :  function (value) {
				if (value >=70) {
					return "Success";
				} else if (value >30 && value <70) {
					return "Warning";
				} else if (value <= 30){
					return "Error";
				} 
		},

		ModeState: function(value){
			if(value === 0) return 'None';
			else  return 'MultiSelect';
			
		}
	};

	return formatter;

},  /* bExport= */ true);